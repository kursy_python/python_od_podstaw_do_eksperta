from pola_figur import pole_trojkata, pole_kwadratu, pole_prostokata
from enum import IntEnum

Menu_Figury = IntEnum('Menu_Figury', {'kwadrat': 1, 'prostokat': 2, 'trojkat': 3, 'zakoncz': 4})
print(list(Menu_Figury))

try_again = "4"
while try_again == "4":
    print("Jakiej figury chcesz obliczyć pole")
    print("""
    1 - kwadrat
    2 - prostokat
    3 - trojkat
    4 - zakończ program
    """)
    pole = 0
    id = int(input("Podaj id figury: "))
    if (id == Menu_Figury.kwadrat):
        a = int(input("Podaj długość boku: "))
        pole = pole_kwadratu(a)
    elif id == Menu_Figury.prostokat:
        a = int(input("Podaj długość boku a : "))
        b = int(input("Podaj długość boku b : "))
        pole = pole_prostokata(a, b)
    elif id == Menu_Figury.trojkat:
        a = int(input("Podaj długość boku a : "))
        h = int(input("Podaj długość wysokości h : "))
        pole = pole_trojkata(a, h)
    elif id == Menu_Figury.zakoncz:
        break
    else:
        print('Podałeś nieznaną opcje.')
    if pole != 0:
        print(f"Pole wynosi {pole}")

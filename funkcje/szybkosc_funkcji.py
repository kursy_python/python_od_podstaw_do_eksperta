import time


def sumuj_do(liczba):
    suma = 0
    for liczba in range(1,liczba+1):
        suma = suma + liczba
    return suma


def sumuj_do2(liczba):
    return sum([liczba for liczba in range(1, liczba+1)])


def function_performance(func, how_many_times=1, *args, **kwargs):
    suma = 0
    for i in range(0, how_many_times):
        start = time.perf_counter()
        func(*args, **kwargs)
        end = time.perf_counter()
        suma = suma + (end - start)
    return suma


#print(function_performance(sumuj_do, 50000))
# print(function_performance(sumuj_do2, 50000))

def find_element(element: int, container):
    if element in container:
        return True
    else:
        False


setContainer = {i for i in range(1000)}
listContainer = (i for i in range(1000))

print(function_performance(find_element, 50000, 450, setContainer))


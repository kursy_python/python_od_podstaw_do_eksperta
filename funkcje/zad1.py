
def count(*args) -> int:
    suma = 0
    for x in args:
        suma = suma + x
    return suma

print(count(1, 2, 3, 22))
"""program sprawdza czy użytkownik jest na liście osób z dostępem"""

user_list = ["Arkadiusz", "Wiola", "Antek"]

user_name = input("Podaj swoje imię: ").capitalize()

if user_name in user_list:
    print(f"Masz dostęp {user_name}!")
else:
    print(f"Nie masz dostępu {user_name}!")

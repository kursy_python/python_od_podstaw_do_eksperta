"""
Program umożliwiający operacje na słowniku:
1 dodawanie definicji
2 szukanie definicji
3 usuwanie definicji
"""

print("""
Dostępne operacje na słowniku:
1 dodawanie definicji
2 szukanie definicji
3 usuwanie definicji
4 zakończenie
""")

definition_dict = {}


while True:
    choice_operation = int(input("Jaką operację chcesz wykonać? "))
    if choice_operation == 1:
        dict_key = input("Podaj słowo klucz do zdefiniowania ")
        if dict_key in definition_dict:
            print(f"Istnieje już definicja dla {dict_key}")
        else:
            dict_def = input("Podaj definicje ")
            definition_dict[dict_key] = dict_def
        if dict_key in definition_dict:
            print("Definicja została dodana")
        else:
            print("Nie udało się dodać nowej definicji!")
    elif choice_operation == 2:
        dict_key = input("Czego szukasz? ")
        if dict_key in definition_dict:
            print(f"{dict_key}: {definition_dict[dict_key]}")
        else:
            print(f"Nie ma definicji w słowniku dla {dict_key}!")
    elif choice_operation == 3:
        dict_key = input("Jaką definicję chcesz usunąć: ")
        if dict_key in definition_dict:
            del definition_dict[dict_key]
            if not dict_key in definition_dict:
                print(f"Usunięto {dict_key} ze słownika.")
        else:
            print(f"Nie znaleziono {dict_key} w słowniku")
    elif choice_operation == 4:
        print("Wyjście")
        break
    else:
        print("Podałeś niepoprawny numer operacji!")

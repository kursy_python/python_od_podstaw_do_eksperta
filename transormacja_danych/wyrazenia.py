"""
ćwiczenia z wyrażeń listownych, słownikowych, geerujących
"""

#wyryżenie listowne

lista = [1, 2, 3, 4, 5]


def potegi_dwojki(lista) -> list:
    potegi = []
    for element in lista:
        potegi.append(element ** 2)
    return potegi


def gen_potegi(lista, potega) -> list:
    potegi = [element**potega for element in lista]
    return potegi


def gen_potegi_parzystych(lista, potega) -> list:
    potegi = [element**potega for element in lista if element % 2 ==0]
    return potegi


# print('potegi_dwojki : ', potegi_dwojki(lista))
#
# print('gen_potegi : ', gen_potegi(lista, 3))
#
# print('gen_potegi_parzystych : ', gen_potegi_parzystych(lista, 3))

# wyrażenia generujące

evenNumberGenerator = (element for element in range(400) if element % 2 != 0)

# for item in evenNumberGenerator:
#     print(item)

#wyrażenie słownikowe

names = {"Arkadiusz", "Wioletta", "Karol", "Bartłomiej", "Jakub"}

numbers = list(range(1, 7, 1))

nameLenght = {
    name: len(names)
    for name in names
    if "K" in name
}

celcius = {'t1': -20, 't2': -15, 't3': 0, 't4': 12, 't5': 24}


def convert_to_fahrenheit(cel):
    fahr = cel * 1.8 + 32
    return fahr


fahrenheit = {
    key: convert_to_fahrenheit(celcius)
    for key, celcius in celcius.items()
    if celcius > -16
}

# wyrażenia zbioru

names = {"Arkadiusz", "Wioletta", "karol", "Bartłomiej", "jakub"}

names = {
    name.capitalize()
    for name in names
}

print(names)
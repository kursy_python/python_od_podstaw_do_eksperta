"""
znajdź liczby od 100 do 470 włącznie, które są:
-podzielne przez 7, ale nie są podzielne przez 5


"""

numbers = list(range(100, 471, 1))

search_num = [
    num for num in numbers
    if num % 7 == 0
    if not num % 5 == 0
]

print(search_num)
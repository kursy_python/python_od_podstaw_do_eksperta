import random

#program do zgadywania liczby

print("Zgadnij liczbę od 1 do 100")

search_number = random.randrange(101)
try_again = "tak"
print(try_again)

while try_again == "tak":
    try:
        x = int(input("Odgadnij liczbę od 0 do 100 : "))
        if x > search_number:
            print("Nie trafiłeś, szukana liczba jest mniejsza")
        elif x < search_number:
            print("Nie trafiłeś, szukana liczba jest większa")
        elif x == search_number:
            print("Brawo! Zgadłeś.")
    except:
        print("Nie podałeś poprawnej liczby")

    try_again = input("Czy chcesdz spróbować jeszcze raz? tak/nie ")
    print(try_again)

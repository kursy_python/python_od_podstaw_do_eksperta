#program który dodaje 3 parzyste liczby podane przez użytkownika

print("""Program sumuje 3 parzyste liczby""")


i = 0
sum_result = 0

while i < 3:
    a = int(input("Podaj liczbę: "))
    if a % 2 == 0:
        sum_result += a
        i += 1

print("Suma podanych 3 parzystych liczb wynosi : ", sum_result)


